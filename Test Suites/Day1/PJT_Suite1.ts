<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>First Test Suite</description>
   <name>PJT_Suite1</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>5</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0886335a-ce8b-440c-8222-9275af2c0ce7</testSuiteGuid>
   <testCaseLink>
      <guid>fdef25d3-6793-47da-9b93-e5f48b12324c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PJT_FirstDay/PJT_ManualTest1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4dd88180-f0a1-4741-8032-ee160329fade</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/PJT_FirstDay/PJT_ScriptTest1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4052710d-2334-42cb-8356-983ebb145add</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/PJT_FirstDay/PJT_TestCase1</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>17fcb9ea-4706-435b-bc9a-9ba9489c5923</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/PJT_FirstDay/Test_RecordMode</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
